# create 5 variables
name = "Emma"
age = 28
occupation = "teacher"
movie = "The Shawshank Redemption"
rating = 9.3

# output the variables in the required format
print(f"lam {name}, and | am {age} years old, | work as a {occupation}, and my rating for {movie} is {rating}%")

# create 3 variables
num1 = 5
num2 = 7
num3 = 10

# get the product of num1 and num2
product = num1 * num2
print(f"The product of {num1} and {num2} is {product}")

# check if num1 is less than num3
if num1 < num3:
    print(f"{num1} is less than {num3}")
else:
    print(f"{num1} is not less than {num3}")

# add the value of num3 to num2
num2 += num3
print(f"The new value of num2 is {num2}")
